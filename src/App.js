import React from "react";
import "./App.css";
import Inner from "./components/InnerComponent";
import Login from "./components/LoginComponent";
import FacebookLoginComponent from "./components/FacebookLoginComponent";

function App() {
  return (
    <div className="App">
      <Inner />
      {/* <FacebookLoginComponent /> */}
    </div>
  );
}

export default App;
