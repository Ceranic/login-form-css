import React, { useState } from "react";
import FacebookLogin from "react-facebook-login";
import { FaFacebook, FaTwitter, FaGoogle } from "react-icons/fa";
import Icon from "@mdi/react";

function FacebookLoginComponent() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  // const [userId, setUserId] = useState("");
  // const [name, setName] = useState("");
  // const [email, setEmail] = useState("");
  // const [picture, setPicture] = useState("");

  const responseFacebook = response => {
    console.log(response);
  };

  const componentClicked = () => {
    console.log("clicked");
  };

  let fbContent;

  if (isLoggedIn) {
    fbContent = null;
  } else {
    fbContent = (
      <FacebookLogin
        appId="834672453648514"
        autoLoad={true}
        fields="name,email,picture"
        onClick={componentClicked}
        callback={responseFacebook}
        // icon="fa-facebook"
      />
    );
  }

  return <div>{fbContent}</div>;
}

export default FacebookLoginComponent;
