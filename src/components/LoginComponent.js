import React, { useRef, useState } from "react";
import { FaFacebook, FaTwitter, FaGoogle } from "react-icons/fa";
import Icon from "@mdi/react";
import { mdiAccountOutline, mdiLockOutline } from "@mdi/js";
import FacebookLogin from "react-facebook-login";
import "./LoginComponent.css";

function LoginComponent() {
  const logAsAdmin = [
    {
      username: "admin",
      password: "admin"
    },
    {
      username: "asd",
      password: "asd"
    }
  ];

  localStorage.setItem("admin", JSON.stringify(logAsAdmin));

  const loginBodyRef = useRef();

  const [spanStyle, setSpanStyle] = useState({
    left: 0,
    top: 0
  });

  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [loginData, setLoginData] = useState({
    username: "",
    password: ""
  });

  const handleUsername = e => {
    setLoginData({ ...loginData, username: e.currentTarget.value });
  };

  const handlePassword = e => {
    setLoginData({ ...loginData, password: e.currentTarget.value });
  };

  const responseFacebook = response => {
    console.log(response);
  };

  const componentClicked = e => {
    let getAdmin = [];
    let getfromLS = JSON.parse(localStorage.getItem("admin"));
    getAdmin.push(...getfromLS);
    console.log(isLoggedIn);

    console.log(
      getAdmin.map(user => {
        if (
          user.username == loginData.username &&
          user.password == loginData.password
        ) {
          return setIsLoggedIn(true);
        } else {
          return setIsLoggedIn(false);
        }
      })
    );
  };
  const changeCords = e => {
    setSpanStyle({
      left: e.clientX - loginBodyRef.current.offsetLeft,
      top: e.clientY - loginBodyRef.current.offsetTop
    });
  };

  // let fbContent;

  // if (isLoggedIn) {
  //   fbContent = null;
  // } else {
  //   fbContent = <FacebookLogin />;
  // }

  return (
    <div
      className="login-body"
      ref={loginBodyRef}
      onMouseMove={e => changeCords(e)}
    >
      <span className="login-circle" style={spanStyle}></span>
      <div className="login-content">
        <h1 className="login-header">Login</h1>
        <div className="login-inputs">
          <div className="login-username">
            <span className="login-user-label">Username</span>
            <Icon
              path={mdiAccountOutline}
              title="User Profile"
              size={1}
              color="#666"
              className="user-icon"
            />
            <input
              type="username"
              className="login-user-input"
              placeholder="Type your username"
              onChange={handleUsername}
            />
          </div>
          <div className="login-password">
            <span className="login-pass-label">Password</span>
            <Icon
              path={mdiLockOutline}
              title="User password"
              size={1}
              className="password-icon"
              color="#666"
            />
            <input
              type="password"
              className="login-user-input"
              placeholder="Type your Password"
              onChange={handlePassword}
            />
            <br />
            <span className="login-forgot-password">Forgot password?</span>
          </div>
          <div className="login-button" onClick={componentClicked}>
            Login
          </div>
          <div className="singup-using">
            <span className="singup-using-span">Or Sign Up using</span>
            <div className="login-logos">
              <FacebookLogin
                appId="834672453648514"
                textButton=""
                icon={<FaFacebook className="facebook logo" />}
                fields="name,email,picture"
                onClick={componentClicked}
                callback={responseFacebook}
                cssClass="fbHide"
              />
              <FaTwitter className="twitter logo" />
              <FaGoogle className="google logo" />
            </div>
          </div>
          <div className="singup-another">Or singup using</div>
          <div className="singup-last">Sing up</div>
        </div>
      </div>
    </div>
  );
}

export default LoginComponent;
