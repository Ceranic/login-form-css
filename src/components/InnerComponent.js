import React from "react";
import "./InnerComponent.css";
import LoginComponent from "./LoginComponent";

function InnerComponent() {
  return (
    <>
      <div className="background-image">
        <LoginComponent />
      </div>
    </>
  );
}

export default InnerComponent;
